import styles from "./style";
import { Compare, Business, CardDeal, Clients, CTA, Footer, 
  Navbar, Stats, Testimonials, Hero } from "./components";
import Packages from "./components/Packages";

const App = () => (
  <div className="bg-primary w-full overflow-hidden">

    <div className={`bg-primary ${styles.flexStart}`}>
      <div className={`${styles.boxWidth}`}>
        <Hero />
      </div>
    </div>

    <div className={`sm:px-16 ${styles.flexCenter} w-full`}>
      <Navbar styles={styles} />
    </div>
    
    <div className={`bg-primary ${styles.paddingX} ${styles.flexCenter}`}>
      <div className={`${styles.boxWidth}`}>
        {/* <Stats /> */}
        <Business />
        <Packages/>
        <Testimonials />
        <Compare />
        {/* <CardDeal /> */}
        {/* <Clients /> */}
        <CTA />
        <Footer />
      </div>
    </div>
  </div>
);

export default App;
