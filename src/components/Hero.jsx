import styles from "../style";
import { discount, robot } from "../assets/_index";
import GetStarted from "./GetStarted";

const Hero = () => {

  const _discount = (
    <div className={`animate-pulse flex-1 ${styles.flexStart} flex-col xl:px-0 sm:px-16 px-6 z-[99] mt-5`}>
      <div className="flex flex-row items-center py-[6px] px-4 bg-discount-gradient rounded-[10px] mb-2">
        <img src={discount} alt="discount" className="w-[32px] h-[32px]" />
        <p className={`${styles.paragraph} ml-2`}>
          <span className="text-white">20% Discount </span>
        </p>
      </div>
    </div>
  )

  return (
    <section
      id="home"
      className={`relative ${styles.paddingY} h-[100vh]
      `}
    >
      {/* Robot Image as Background */}
      <div className="absolute inset-0 overflow-hidden flex justify-center items-end">
        <img src={robot} alt="robot" className="object-contain sm:h-full h-[50%] " />
      </div>

      <div className={`
        ${styles.boxWidth} relative z-10`}>
        <div className={`flex flex-col xl:px-0 sm:px-16 px-6`}>
          <div className="flex flex-row justify-between       
              items-center w-full 
              ss:text-[120px] text-[75px]
              ss:leading-[110px] leading-[80px]">
            <h1 className="flex-1 font-poppins font-semibold
             text-white text-shadow sm:mt-[65px]">
              Nak cari <br className="" />{" "}
              <span className="text-gradient text-shadow-white">
                Kad<br/>Kahwin</span>?
            </h1>

            <a href="#_preview">
              <div className="animate-bounce ss:flex hidden md:mr-4 mr-0 opacity-90">
                <GetStarted />
              </div>
            </a>
          </div>

          {/* <h1 className="font-poppins font-semibold ss:text-[68px] text-[52px] text-white ss:leading-[100.8px] leading-[55px] w-full">
            mantap?
          </h1> */}
          <p className={`font-poppins font-normal text-white text-[18px] sm:leading-[30px] 
          leading-[22px] max-w-[470px] mt-5 text-shadow bg-black-gradient-2 p-2 rounded-md
          sm:w-[500px]`}>
            Murah, moden, menawan!
            <br />
            <br/>
            Biar orang tahu Kad Kahwin anda moden & cantik! 
            <br/>
            <span className="opacity-80 sm:text-[18px] text-[12px]">
              Apa lagi? Tayang kat jiran! Showoff kat ex!</span>
          </p>
        </div>

        {/* Discount  */}
        <a href="https://api.whatsapp.com/send?phone=60134806952&text=Hai%21%20Saya%20nak%20kad%20kahwin%20DP20" target="_blank">
          {_discount}
        </a>
      </div>

      {/* GetStarted at the Bottom */}
      <a href="#_preview">
        <div className="absolute bottom-0 left-0 right-0
          animate-bounce opacity-90  ss:hidden flex
         justify-center items-end pb-6">
          <GetStarted />
        </div>
      </a>
    </section>
  );
};

export default Hero;
