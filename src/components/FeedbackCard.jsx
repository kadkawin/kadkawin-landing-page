import { quotes } from "../assets/_index";

const FeedbackCard = ({ content, name, title, img, set, price }) => {
  let bg, bgprice;
  switch (set) {
    case "package_1":
      bg = "bg-gray-gradient";
      bgprice = "bg-red-800";
      break;
    case "package_2":
      bg = "bg-black-gradient-2";
      bgprice = "bg-red-700";
      break;
    case "package_3":
      bg = "bg-nav-gradient text-shadow";
      bgprice = "bg-red-600";
      break;
    case "testimoni":
      bg = "bg-black-gradient-2 text-shadow";
      break;
    default:
      bg = 'feedback-card';
  }

  return (
    <div className={`relative flex flex-col sm:px-5 px-4 py-2 rounded-[20px] max-w-[370px] md:mr-10 sm:mr-5 mr-0 my-2 ${bg} fade-in-fast text-shadow`}>
      {price && (
        <div className={`animate-pulse absolute top-2 right-2 flex flex-col items-center justify-center w-20 h-20 bg-discount-gradient text-white font-semibold rounded-full text-[25px] leading-none text-center`}>
          <span>RM</span>
          <span className="text-[40px]">{`${price.split(' ')[1]}`}</span>
        </div>
      )}

      <div className="flex flex-row">
        {!set.includes('package') ? (
          <img src={img} alt={name} className="w-[48px] h-[48px] rounded-full" />) : ''}
        <div className="flex flex-col ml-4 text-shadow">
          <h4 className={`text-gradient font-poppins font-semibold ${set.includes('package') ? 'text-[25px]' : 'text-[20px]'} leading-[32px] text-white`}>
            {name}
          </h4>
          <p className="font-poppins font-normal text-[16px] leading-[24px] text-dimWhite">
            {title}
          </p>
        </div>
      </div>

      {/* <img src={quotes} alt="double_quotes" className="w-[42.6px] h-[27.6px] object-contain" /> */}
      <p
        style={{ whiteSpace: "pre-line" }}
        className="font-poppins font-normal text-[18px] leading-[32.4px] text-white my-2">
        {content}
      </p>
    </div>
  );
};

export default FeedbackCard;