import { useState,useEffect } from "react";
import styles, {layout} from "../style";
import Button from "./Button";
import { whatsapp } from "../assets/_index";

const CTA = () => {
  const [minDate, setMinDate] = useState('');
  const [selectedDate, setSelectedDate] = useState('');

  useEffect(() => {
    // Calculate tomorrow's date
    const tomorrow = new Date();
    tomorrow.setDate(tomorrow.getDate() + 1);

    // Set the initial minimum and selected date to tomorrow
    setMinDate(tomorrow.toISOString().split('T')[0]);
    setSelectedDate(tomorrow.toISOString().split('T')[0]);
  }, []);

  const handleDateChange = (event) => {
    // Update the selected date
    setSelectedDate(event.target.value);

    // Check if the selected date is today
    const today = new Date().toISOString().split('T')[0];
    if (event.target.value === today) {
      // If selected date is today, calculate and set the minimum date for tomorrow
      const tomorrow = new Date();
      tomorrow.setDate(tomorrow.getDate() + 1);
      setMinDate(tomorrow.toISOString().split('T')[0]);
    } else {
      // If selected date is not today, set the minimum date to the default
      setMinDate(event.target.value);
    }
  };

  // Calculate tomorrow's date
  // const tomorrow = new Date();
  // tomorrow.setDate(tomorrow.getDate() + 1);

  const handleSubmit = (e) => {
    e.preventDefault();

    const brideName = document.getElementById('brideName').value;
    const groomName = document.getElementById('groomName').value;
    const weddingDate = document.getElementById('weddingDate').value;
    const hashtag = document.getElementById('hashtag').value;

    // Construct the URL with parameters
    const url = `https://preview.kahwin.link/preview?bride=${brideName}&groom=${groomName}&tDate=${weddingDate}&hashtag=${hashtag.replace('#','')}`;

    // Redirect to the constructed URL
    window.location.href = url;
  };
return (
  <section id="_preview"
    className={`${styles.flexCenter} ${styles.marginY} ${styles.padding} sm:flex-row flex-col bg-black-gradient-2 rounded-[20px] box-shadow`}>
    <div className="flex-1 flex flex-col">
      <h2 className={`font-poppins font-semibold xs:text-[48px] text-[40px] text-white xs:leading-[76.8px] sm:leading-[66.8px] leading-[50px] w-full`}>Tunggu lagi? 
        <br/><span className="text-gradient">Cuba</span> lah!
      </h2>
      <p className={`${styles.paragraph} max-w-[470px] mt-2`}>
        Tengok sendiri apa yang ada! <br/>
        Untuk lebih lanjut : {"  "}
        <span className="text-green-500 sm:inline block">
          Whatsapp <img src={whatsapp} alt="whatsapp" className="w-[25px] h-[25px] inline-flex mb-2 animate-pulse" />
        </span>
      </p>
    </div>

    <div className={`${styles.flexCenter} flex-1 mt-0`}>
      <form className="w-full" action="" method="post" 
          onSubmit={handleSubmit}>
          <div className="flex flex-1 w-[100%]">
            <input
              id="brideName"
              name="brideName"
              type="text"
              className={`${layout.input100} ${layout.inputText}`}
              placeholder="Pasangan Wanita ( cth: Sofea )"
              onChange={(e) => {
                e.target.value = e.target.value.replace(/[^'@/a-zA-Z]/g, "");
              }}
            />
          </div>
          <div className="flex flex-1 w-[100%]">
            <input
              id="groomName"
              name="groomName"
              type="text"
              className={`${layout.input100} ${layout.inputText}`}
                placeholder="Pasangan Lelaki  ( cth: Faris )"
                onChange={(e) => {
                  e.target.value = e.target.value.replace(/[^'@/a-zA-Z]/g, "");
                }}
            />
          </div>
          <div className="flex flex-1 w-[100%]">
            <label htmlFor="weddingDate" className="text-white my-2">Tarikh Majlis 
            {/* <span className="ss:block hidden mr-2">( cth: {selectedDate} )</span> */}
            </label>
              <input
                id="weddingDate"
                name="weddingDate"
                type="date"
                className={`w-full ${layout.inputText}`}
                // min={tomorrow.toISOString().split('T')[0]}
                min={minDate}
                value={selectedDate}
                onChange={handleDateChange}
              />
          </div>
          <div className="flex flex-1 w-[100%]">
            <input
              id="hashtag"
              name="hashtag"
              type="text"
              className={`${layout.input100} ${layout.inputText}`}
                placeholder="Hashtag?  ( cth: #SoFarToSofea )"
                onChange={(e) => {
                  e.target.value = "#" + e.target.value;
                  if((e.target.value).includes("##")){
                    e.target.value = (e.target.value).replace("##","#");
                  }
                }}
            />
          </div>
        <Button 
          text="Jum tengok!" 
          styles={`float-right mt-2`} />
      </form>
    </div>
  </section>
)};

export default CTA;
