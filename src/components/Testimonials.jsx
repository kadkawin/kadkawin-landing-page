import { feedback } from "../constants";
import styles from "../style";
import FeedbackCard from "./FeedbackCard";

const Testimonials = () => (
  <section id="_testimoni" className={`${styles.paddingY} ${styles.flexCenter} flex-col relative `}>
    <div className="absolute z-[0] w-[60%] h-[60%] -right-[50%] rounded-full blue__gradient bottom-40" />

    <div className="w-full flex justify-between items-center flex-col sm:mb-0 mb-6 relative z-[1]">
      <h2 className={styles.heading2}>
        Tak <span className="text-gradient">percaya?</span>
      </h2>
      <div className="w-full">
        <p className={`${styles.paragraph} text-left max-w-[450px]`}>
          Tengok apa orang lain cakap!
        </p>
      </div>
    </div>

    <div className="flex flex-wrap sm:justify-start justify-center w-full feedback-container relative z-[1]">
      {feedback.map((card) => <FeedbackCard key={card.id} {...card} />)}
    </div>
  </section>
);

export default Testimonials;
