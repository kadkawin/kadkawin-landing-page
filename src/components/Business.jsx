import { features } from "../constants";
import styles, { layout } from "../style";
import Button from "./Button";

const FeatureCard = ({ icon, title, content, index }) => (
  <div className={`flex flex-row p-6 rounded-[20px] w-full
  ${index !== features.length - 1 ? "mb-6" : "mb-0"} bg-nav-gradient`}>
    <div className={`w-[64px] h-[64px] rounded-full ${styles.flexCenter} bg-dimBlue`}>
      <img src={icon} alt="star" className="w-[50%] h-[50%] object-contain" />
    </div>
    <div className="flex-1 flex flex-col ml-3">
      <h4 className="font-poppins font-semibold text-white text-[18px] leading-[23.4px] mb-1">
        {title}
      </h4>
      <p className="font-poppins font-normal text-shadow text-white text-[16px] leading-[24px]">
        {content}
      </p>
    </div>
  </div>
);

const Business = () =>  (
  <section id="features" className={layout.section}>
    <div className={`${layout.sectionInfo} sm:flex-row flex-col items-center sm:space-x-10`}>
      <h2 className={`font-poppins font-semibold xs:text-[46px] text-[40px] text-white xs:leading-[76.8px] sm:leading-[66.8px] leading-[50px] sm:w-3/5`}>
        <span className="text-gradient">Senang je! </span>
        <br />
        🎨 Pilih design<br />
        ✅ Order<br />
        📨 Siap!
      </h2>
      <div className="sm:w-2/5">
        <p className={`font-poppins font-normal text-white text-[18px] leading-[30.8px] max-w-[470px] mt-5`}>
          Taktau nak pilih design? <span className="text-gradient">Order dulu!</span>
          <br />
          Nak harga paling bajet? <span className="text-gradient">Boleh!</span>
          <br />
          Ada design sendiri? <span className="text-gradient">Onz!</span>
          <br />
        </p>
        <a href="https://api.whatsapp.com/send?phone=60134806952&text=Hai%21%20Saya%20nak%20kad%20kahwin%20SEKARANG%21" target="_blank">
          <Button text='Order sekarang!' styles={`sm:mt-10 mt-5`} />
        </a>
      </div>
    </div>

    <div className={`${layout.sectionImg} text-shadow flex-col`}>
      {features.map((feature, index) => (
        <FeatureCard key={feature.id} {...feature} index={index} />
      ))}
      
      <span className="opacity-80 text-left text-white sm:text-[15px] text-[12px]">
        *Tertakluk kepada terma dan syarat
       </span>
    </div>
  </section>

);

export default Business;
