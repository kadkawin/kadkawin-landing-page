import { useState,useEffect } from "react";

import { close, logo, discount, menu } from "../assets/_index";
import { navLinks } from "../constants";

const Navbar = ({styles}) => {
  const [active, setActive] = useState("Home");
  const [toggle, setToggle] = useState(false);  
  const [sticky, setSticky] = useState(false);

  const handleScroll = () => { 
    const scrollPosition = window.scrollY;
    setSticky(scrollPosition > window.innerHeight - 500);
  };
  
  useEffect(() => {
    setSticky(false);
    window.addEventListener("scroll", handleScroll);

    return () => {
      window.removeEventListener("scroll", handleScroll);
    };
  }, []); // Add _url as a dependency to useEffect
  
  const _discount = (
    <div className={`flex flex-row items-center 
    py-[6px] px-4 bg-discount-gradient rounded-[10px]
    mr-4 
    ${!toggle ? '' : ''}`}>
      <img src={discount} alt="discount" className="w-[32px] h-[32px]" />
      <p className={`font-poppins font-normal text-dimWhite 
      text-[12px] leading-[30.8px] ml-2`}>
        <span className="text-white">Promosi 20%</span>
      </p>
    </div>
  )

  return (
    <div 
      key={sticky}
      className={`${styles.boxWidth} 
      ${sticky ? 'px-2 fixed top-0 z-50' : 'hidden'} text-shadow bg-nav-gradient fade-in-fast`}>
      <nav className="w-full flex py-2 justify-between items-center navbar">
        <img src={logo} alt="kadkawin" className="w-[140px] h-[auto]" />

        <ul className="list-none sm:flex hidden justify-end items-center flex-1">
          {navLinks.map((nav, index) => (
            <li
              key={nav.id}
              className={`font-poppins font-normal cursor-pointer text-[16px] ${
                active === nav.title ? "text-white" : "text-dimWhite"
              } ${index === navLinks.length - 1 ? "mr-0" : "mr-10"}`}
              onClick={() => setActive(nav.title)}
            >
            <a href={`${nav.link}`}>{nav.title}</a>
            </li>
          ))}
        </ul>

        <div className="sm:hidden flex flex-1 justify-end items-center">
          <a href="https://api.whatsapp.com/send?phone=60134806952&text=Hai%21%20Saya%20nak%20kad%20kahwin%20DP20" target="_blank">
            {_discount}
          </a>
          <img
            src={toggle ? close : menu}
            alt="menu"
            className="w-[28px] h-[28px] object-contain"
            onClick={() => setToggle(!toggle)}
          />

          <div
            className={`${
              !toggle ? "hidden" : "flex"
            } p-6 bg-black-gradient absolute top-20 right-0 mx-4 my-2 min-w-[140px] rounded-xl sidebar`}
          >
            <ul className="list-none flex justify-end items-start flex-1 flex-col">
              {navLinks.map((nav, index) => (
                <li
                  key={nav.id}
                  className={`font-poppins font-medium cursor-pointer text-[16px] ${
                    active === nav.title ? "text-white" : "text-dimWhite"
                  } ${index === navLinks.length - 1 ? "mb-0" : "mb-4"}`}
                  onClick={() => setActive(nav.title)}
                >
                  <a href={`${nav.link}`}>{nav.title}</a>
                </li>
              ))}
            </ul>
          </div>
        </div>
      </nav>
    </div>
  );
};

export default Navbar;
