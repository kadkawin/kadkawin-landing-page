import { apple, google } from "../assets/_index";
import styles, { layout } from "../style";
import { specifications, ourData, theirData } from "../constants";

const Billing = () => (
  <section id="product" className={layout.sectionReverse}>
    <div className={layout.sectionImgReverse}>
      {/* <img src={bill} alt="billing" className="w-[100%] h-[100%] relative z-[5]" /> */}
      <div className="mx-auto max-w-screen-90vw sm:text-[20px] text-[14px] p-8 bg-black-gradient-2 shadow-md rounded-md">
      <div className="overflow-hidden">
        <table className="min-w-full divide-y divide-gray-200">
          <thead className="bg-gray-50">
            <tr>
              <th scope="col" className="max-w-[10%] px-3 py-3 text-left text-xs font-medium text-gray-500 uppercase tracking-wider opacity-0 bg-none">
                <span className=""></span>
              </th>
              <th scope="col" className="w-[42.5%] px-6 py-3 text-left sm:text-[20px] text-[15px] font-bold text-black uppercase tracking-wider bg-nav-gradient">
                <span className="">KadKahwin</span>
                {/* <span className="hidden sm:inline">Our Brand</span> */}
              </th>
              <th scope="col" className="w-[42.5%] px-6 py-3 text-left  sm:text-[20px] text-[15px] font-medium text-gray-500 uppercase tracking-wider">
                <span className="">Kad lain</span>
                {/* <span className="hidden sm:inline">Competitor</span> */}
              </th>
            </tr>
          </thead>
          <tbody className="bg-white divide-y divide-gray-200">
            {specifications.map((spec, index) => (
              <tr key={index}>
                <td className="max-w-[10%] px-3 sm:py-4 py-2 whitespace-wrap">{spec}</td>
                <td className="w-[42.5%] sm:px-6 px-2 sm:py-4 py-2 font-medium whitespace-pre-wrap bg-nav-gradient">{ourData[index]}</td>
                <td className="w-[42.5%] sm:px-6 px-2 sm:py-4 py-2 whitespace-pre-wrap">{theirData[index]}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </div>
    </div>

      {/* gradient start */}
      <div className="absolute z-[3] -left-1/2 top-0 w-[50%] h-[50%] rounded-full white__gradient" />
      <div className="absolute z-[0] w-[50%] h-[50%] -left-1/2 bottom-0 rounded-full pink__gradient" />
      {/* gradient end */}
    </div>

    <div className={layout.sectionInfo}>
      <h2 className={styles.heading2}>
        <span className="text-gradient">Beza kami</span> 
        <br className="sm:block hidden" /> dengan yang lain?
      </h2>
      <p className={`${styles.paragraph} text-white  max-w-[470px] mt-5`}>
        <span className="text-gradient">Harga?</span> Yang lain sampai RM200 😨<br/>
        <span className="text-gradient">Design?</span> Kami lagi lawa! ✨<br/>
        <span className="text-gradient">Fungsi?</span> Kami macam-macam ada! 🎉<br/>
      </p>

      {/* <div className="flex flex-row flex-wrap sm:mt-10 mt-6">
        <img src={apple} alt="google_play" className="w-[128.86px] h-[42.05px] object-contain mr-5 cursor-pointer" />
        <img src={google} alt="google_play" className="w-[144.17px] h-[43.08px] object-contain cursor-pointer" />
      </div> */}
    </div>
  </section>
);

export default Billing;
