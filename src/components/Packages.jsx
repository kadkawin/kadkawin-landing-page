import { packages } from "../constants";
import styles from "../style";
import FeedbackCard from "./FeedbackCard";

const Packages = () => (
  <section id="_package" 
  className={`sm:py-10 py-6 ${styles.flexCenter} flex-col relative `}>
    {/* <div className="absolute z-[0] w-[60%] h-[60%] -right-[50%] rounded-full blue__gradient bottom-40" /> */}

    <div className="w-full flex justify-between items-center flex-col  relative z-[1]">
      <h2 className={styles.heading2}>
        Pakej Kami
      </h2>
      <div className="w-full md:mt-0 mt-1">
        <p className={`${styles.paragraph} text-left max-w-[450px]`}>
          Paling murah ada, add-on ikut budget anda!
          Nak premium pun boleh!
        </p>
      </div>
    </div>

    <div className="flex flex-wrap sm:justify-start justify-center w-full feedback-container relative z-[1]">
      {packages.map((card) => <FeedbackCard key={card.id} {...card} />)}
    </div>
  </section>
);

export default Packages;
