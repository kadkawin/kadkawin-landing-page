import { testi1,testi2,testi3, 
  facebook, instagram, linkedin, twitter, send, shield, star  } from "../assets/_index";

export const navLinks = [
  {
    id: "home",
    title: "Home",
    link: "#"
  },
  {
    id: "package",
    title: "Pakej Kami",
    link: "#_package"
  },
  {
    id: "testimoni",
    title: "Bukti!",
    link: "#_testimoni"
  },
  {
    id: "preview",
    title: "Cuba try!",
    link: "#_preview"
  },
];

export const stats = [
  {
    id: "stats-1",
    title: "Pelanggan seluruh Malaysia",
    value: "500+",
  },
  {
    id: "stats-2",
    title: "Design Kad",
    value: "100+",
  },
  {
    id: "stats-3",
    title: "Paling murah!",
    value: "RM ?",
  },
];

export const features = [
  {
    id: "feature-1",
    icon: star,
    title: "🎨 Pilih design",
    content:
      `Taktau nak pilih mana satu? Takpe, proceed order dulu! Pilih nanti je`,
  },
  {
    id: "feature-2",
    icon: shield,
    title: "✅ Order",
    content:
      "Kami terima sebarang bentuk pembayaran. Paling murah di pasaran!",
  },
  {
    id: "feature-3",
    icon: send,
    title: "📨 Siap!",
    content:
      "Dijamin siap dalam masa 24jam!",
  },
];

export const packages = [
  {
    id: "package-1",
    content:
     `1) 2 Basic Pages 📄🎉 (Mukadepan, RSVP)
      2) Senarai Kehadiran 📋
      3) Limited design 🎨
      4) Countdown Timer 🕖
      5) Lokasi (Google/Waze) 🗺️
      6) Kalendar (Googe/Apple) 📅
      7) Direct call/whatsapp ☎️📱
      8) Gambar pengantin 🤵👰

      PERCUMA:
      9) QR code untuk pengantin 💵
      10) Ubah ayat atau info ✏️📝
      11) Problem Support 🤝🛠️ [Unlimited]

      Add On (Extra charge):
      * +Page Jemputan 💌
      * +Page Aturcara 📆
      * +Mesej untuk Pengantin 📩
      * +Album Gambar [50 slot] 📸
      
      *Kad Kahwin 60 hari dari Tarikh Jemputan 📆💒`,
    name: "Basic",
    title: "Harga asal: RM 30",
    // img: people01,
    price: "RM 15",
    set: 'package_1'
  },
  {
    id: "package-2",
    content:
     `1) 2 Basic Pages 📄🎉 (Mukadepan, RSVP)
      2) 2 Extra Pages 🎁🌟 (Jemputan, Aturcara)
      3) Senarai Kehadiran 📋
      4) Boleh guna background sendiri! 🎨
      5) Boleh guna warna pilihan! 🌈
      6) Album Gambar 📸
      7) Countdown Timer 🕖
      8) Lokasi (Google/Waze) 🗺️
      9) Kalendar (Googe/Apple) 📅
      10) Direct call/whatsapp ☎️📱
      11) Gambar pengantin 🤵👰
      
      PERCUMA (Freebies):
      12) QR code untuk pengantin 💵
      13) Ubah ayat atau info ✏️📝
      14) Problem Support 🤝🛠️ [Unlimited]
      15) +Font pilihan ✒️📝 (Daripada Google Fonts)
      16) +Mesej untuk Pengantin 📩
      17) +Album Gambar 30 hari - 100 slot 📸
      
      *Kad Kahwin 120 hari dari Tarikh Jemputan 📆💒`,

    name: "Bajet",
    title: "Harga asal: RM 80",
    // img: people02,
    price: "RM 50",
    set: 'package_2'
  },
  {
    id: "package-3",
    content:
     `1) 2 Basic Pages 📄🎉 (Mukadepan, RSVP)
      2) 2 Extra Pages 🎁🌟 (Jemputan, Aturcara)
      3) Unlimited Page! 📄🌐
      4) Design Premium 🌟
      5) Senarai Kehadiran 📋
      6) Album Gambar 📸
      7) Boleh guna background sendiri! 🎨
      8) Boleh guna design sendiri! ✨
      9) Boleh guna warna pilihan! 🌈
      10) Countdown Timer 🕖
      11) Lokasi (Google/Waze) 🗺️
      12) Kalendar (Googe/Apple) 📅
      13) Direct call/whatsapp ☎️📱
      14) Gambar pengantin 🤵👰
      
      PERCUMA (Freebies):
      15) QR code untuk pengantin 💵
      16) Ubah ayat atau info ✏️📝
      17) Problem Support 🤝🛠️ [Unlimited]
      18) +Font pilihan ✒️📝 (Daripada Google Fonts)
      19) +Mesej untuk Pengantin 📩
      20) +Album Gambar unlimited 📸
      
      *Kad Kahwin bila-bila masa dari Tarikh Jemputan 📆💒
      `,

    name: "Premium",
    title: "Harga asal: RM 200",
    // img: people03,
    price: "RM 99",
    set: 'package_3'
  },
];

export const feedback = [
  {
    id: "feedback-1",
    content:
      "Cantik gila! Memang puas hati sangat-sangat! Semua orang puji iolss punya kad",
    name: "Dini & Ameer",
    title: "Majlis - 2 Julai 2022",
    img: testi1,
    set: 'testimoni'
  },
  {
    id: "feedback-2",
    content:
      "Layanan memang terbaik... terima kasih layan karenah kami mintak tukar macam-macam... professional!!",
    name: "Rawiyah & Faris",
    title: "Majlis - 2 Disember 2023",
    img: testi2,
    set: 'testimoni'
  },
  {
    id: "feedback-3",
    content:
      "Dah book siap-siap lama dah. Memang dah aim kad kawin dari harituu lagiii hehehe",
    name: "Izza & Muaz",
    title: "Majlis - 11 Februari 2024",
    img: testi3,
    set: 'testimoni'
  },
];

export const footerLinks = [
  {
    title: "Link lain-lain",
    links: [
      {
        name: "Preview",
        link: "https://www.preview.com/preview?bride=Bride&groom=Groom&tDate=2024-12-31&hashtag=LoveHashtag",
      },
    ],
  },
  {
    title: "Hubungi Kami",
    links: [
      {
        name: "Pengurus",
        link: "https://api.whatsapp.com/send?phone=60193767697",
      },
      {
        name: "Admin",
        link: "https://api.whatsapp.com/send?phone=60134806952",
      },
    ],
  },
  {
    title: "Partner",
    links: [
      {
        name: "Partner Kami (TBD)",
        link: "#",
      },
      {
        name: "Join Jadi Partner! (TBD)",
        link: "#",
      },
    ],
  },
];

export const socialMedia = [
  {
    id: "social-media-1",
    icon: instagram,
    link: "https://www.instagram.com/",
  },
  {
    id: "social-media-2",
    icon: facebook,
    link: "https://www.facebook.com/",
  },
  {
    id: "social-media-3",
    icon: twitter,
    link: "https://www.twitter.com/",
  },
  // {
  //   id: "social-media-4",
  //   icon: linkedin,
  //   link: "https://www.linkedin.com/",
  // },
];

export const clients = [
  // {
  //   id: "client-1",
  //   logo: airbnb,
  // },
  // {
  //   id: "client-2",
  //   logo: binance,
  // },
  // {
  //   id: "client-3",
  //   logo: coinbase,
  // },
  // {
  //   id: "client-4",
  //   logo: dropbox,
  // },
];

export const specifications = [
  'Harga',
  'Design',
  'Susunan',
  'Sistem RSVP', //4
  'Page RSVP',
  'Album',
  'Font',
  'Countdown', //8
];

export const ourData = [
  'Serendah RM15!', 
  'Modern, Pastel, Jawi, semua ada!', 
  `🟢 Sebijik kad kahwin real
🟢 Teratur kemas 
🟢 4 muka surat`, 
  'Page ASING khas untuk pengantin', 
    `🟢 List nama 
🟢 Bilangan setiap kehadiran
🟢 Boleh Call & Whatsapp`,
   'Tetamu boleh upload dengan ucapan sekali 📸', 
   'Boleh tukar ✨', 
   'Minimal, elegan, cantik', 
  ];
export const theirData = [
  'RM50 - RM100', 
  'Design canva basic', 
  'Semua letak satu tempat', 
  'Satu kampung boleh tengok 👀', 'Hanya kira bilangan...',
   'Tiada 😖', 
   'Tak boleh tukar', 
   'Tak boleh ubah', 
  ];
